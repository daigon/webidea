import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';
const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/login' },
    {
        path: 'manageUsers',
        loadChildren: './components/ManageUsers/ManageUsers.module#ManageUsersModule',
    },
    { path: 'login', loadChildren: './components/Login/Login.module#LoginModule' },
    { path: 'api/verify/:token', loadChildren: './components/ResetPassword/ResetPassword.module#ResetPasswordModule' },
   { path: 'showIdeas', loadChildren: './components/ShowIdeas/ShowIdeas.module#ShowIdeasModule',canActivate:[AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
