import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

// configuracion 
import { GLOBAL } from './global';

// Models
import { Idea } from '../models/idea';
import { Image } from '../models/image';
import { Comment } from '../models/comment';
import { Review } from '../models/review';
import { notify } from '../models/notify';

@Injectable()
export class IdeaService {

	constructor(private _http: Http) { } 

	public AddIdea(token, idea: Idea) {
		return this._http.post(GLOBAL.urlCreateIdea, idea, this.generateHeaders(token))
			.map(res => res.json());
	}

	public AddImage(token, image: Image) {
		return this._http.post(GLOBAL.urlCreateImage, image, this.generateHeaders(token))
			.map(res => res.json());
	}

	public UpdateIdea(token, idea: Idea, id: string) {
		return this._http.put(GLOBAL.urlIdea + id, idea, this.generateHeaders(token))
			.map(res => res.json());
	}

	public UpdateNotify(token = null, notify:notify, id:string) {
		return this._http.put(GLOBAL.urlNotify + id, notify, this.generateHeaders(token))
		.map(res => res.json());
	}


	public GetIdea(token, id) {
		return this._http.get(GLOBAL.urlIdea + id, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetIdeasByScore(token, page = null) {
		return this._http.get(GLOBAL.urlIdeasScore + page, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetIdeasByLikes(token, page: number = null) {
		return this._http.get(GLOBAL.urlIdeasLikes + page, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetIdeasByDate(token, page: number = null) {
		return this._http.get(GLOBAL.urlIdeasDate + page, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetIdeasByAuthor(id: string, object, token) {
		return this._http.post(GLOBAL.urlIdeasAuthor + id, object, this.generateHeaders(token))
			.map(res => res.json());
	}
	public GetIdeasBySearchAndAuthor(object, idAuthor, token) {
		return this._http.post(GLOBAL.urlIdeasSearchAndAuthor + idAuthor, object, this.generateHeaders(token))
			.map(res => res.json());
	}
	public GetIdeasBySearch(page, object, token) {
		return this._http.post(GLOBAL.urlIdeasSearch + page, object, this.generateHeaders(token))
			.map(res => res.json());
	}


	public GetCoach(token, page: number) {
		return this._http.get(GLOBAL.urlCoach + page, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetCollaborators(token, page: number) {
		return this._http.get(GLOBAL.urlCollaborators + page, this.generateHeaders(token))
			.map(res => res.json());
	}


	private generateHeaders(token) {
		const headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});
		return new RequestOptions({ headers });
	}

	public GetImage(token, image) {
		return this._http.get(GLOBAL.urlGetIdeaImage + image, this.generateHeaders(token))
			.map(res => res.json());
	}


}