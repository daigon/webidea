const ip= "http://192.168.15.13:8080";
export var GLOBAL = {
    //idea Routes
    ipweb: "https://api-res-daigon.herokuapp.com",
    iplocal: "http://192.168.15.13:8080",
    urlCreateIdea: `${ip}/api/idea/register`,
    urlCreateImage: `${ip}/api/idea/image`,
    urlIdea: `${ip}/api/idea/`,
    urlIdeas: `${ip}/api/ideas/`,

    urlIdeasAuthor: `${ip}/api/ideas/author/`,
    urlIdeasScore: `${ip}/api/ideas/score/`,
    urlIdeasSearchAndAuthor: `${ip}/api/ideas/search/author/`,
    urlIdeasSearch: `${ip}/api/ideas/search/`,
    urlIdeasDate: `${ip}/api/ideas/date/`,
    urlIdeasLikes: `${ip}/api/ideas/likes/`,

    urlGetIdeaImage: `${ip}/api/idea/get-image-idea/`,
    urlUpIdeaImage: `${ip}/api/upload-image-idea/`,

    urlComments: `${ip}/api/idea/comments/`,
    urlUpdateLikes: `${ip}/api/idea/updatelikes/`,
    urlNotify: `${ip}/api/idea/updateNotify/`,


    //User routes
    urlLoginMovil: `${ip}/api/movil/`,
    urlLoginWeb: `${ip}/api/web/`,
    urlGetUserData:`${ip}/api/user/`,
    urlUsers:`${ip}/api/users/`,
    urlCoach: `${ip}/api/coach/`,
    urlCollaborators: `${ip}/api/colaborator/`,
    
    urlRegisterUser:`${ip}/api/user/register/`,
    urlUpdateUser:`${ip}/api/user/update/`,
    urlDeleteUsers: `${ip}/api/user/`,
    urlGetUsersSearch: `${ip}/api/users/search/`,
    urlGetUsersSearchRole: `${ip}/api/users/searchRole/`,
    urlUploadImageUser:`${ip}/api/user/upload-image-user/`,
    urlGetImageUser: `${ip}/api/user/get-image-user/`,
   
    urlVerifyData: `${ip}/api/user/verifyData`,
    urlVerifyEmail: `${ip}/api/user/verifyEmail`,
    urlForgotPassword: `${ip}/api/user/forgot`,
    urlResetPassword: `${ip}/api/reset/`,
    urlWorks: `${ip}/api/wendy/create/`,
    urlDeleteWorks: `${ip}/api/wendy/delete/`,
    urlUpdateWorks: `${ip}/api/wendy/update/`,
    urlGetWorks: `${ip}/api/wendy/getworks/`
}

