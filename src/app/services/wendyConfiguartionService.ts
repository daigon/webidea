import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

// configuracion
import { GLOBAL } from './global';

@Injectable()
export class WendyConfiguartionService {

    constructor(private _http: Http) { }

    public AddWorks(token, object) {
        return this._http.post(GLOBAL.urlWorks, object, this.generateHeaders(token))
            .map(res => res.json());
    }

    public DeleteWorks(token, obeject) {
        return this._http.post(GLOBAL.urlDeleteWorks, obeject, this.generateHeaders(token))
            .map(res => res.json());
    }

    public UpdateWorks(token, obeject) {
        return this._http.put(GLOBAL.urlUpdateWorks, obeject, this.generateHeaders(token))
            .map(res => res.json());
    }

    public GetWorks(token) {
        return this._http.get(GLOBAL.urlGetWorks , this.generateHeaders(token))
        .map(res => res.json());
    }

    private generateHeaders(token) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': token
        });
        return new RequestOptions({ headers });
    }
}
