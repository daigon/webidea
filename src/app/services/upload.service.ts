
import { Injectable } from '@angular/core';
import { Http, Response, Headers,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class UploadService {
    
    constructor(private _http: Http) {
        
    }
    makeFileRequest( params: Array<string>, files: Array<File>, token: string, name: string,id,image:string ) {
        
    
        return new Promise( function(resolve,reject) {
          const formData: any = new FormData();
          const xhr = new XMLHttpRequest();
    
          for(var i= 0; i < files.length; i++) {
            formData.append(name, files[i], files[i].name);
          }
          console.log(formData);
          xhr.onreadystatechange = () => {
            if( xhr.readyState == 4) {
              if(xhr.status === 200) {
                resolve(JSON.parse(xhr.response));
              }
              else {
                reject(xhr.response);
              }
            }
          }
          xhr.open('POST', GLOBAL.urlUploadImageUser + id, true);
          xhr.setRequestHeader('Authorization', token);
          xhr.setRequestHeader('oldimage', image);
          xhr.send(formData);
        });
      }
   
}