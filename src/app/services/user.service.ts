import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { GLOBAL } from './global';


@Injectable()
export class UserService {
	public url: String;
	public identity: User;
	public token;
	public isLogged: boolean = false;
	constructor(private _http: Http) {
		if (localStorage.getItem('isLoggedin') === 'true') {

			this.identity = this.getIdentity();
			this.identity.image = null;
			this.identity.image = this.getIdentity().image;
			this.isLogged = true;
		}
	}

	private generateHeaders(token) {
		const headers = new Headers({
			'Content-Type': 'application/json',
			'Authorization': token
		});
		return new RequestOptions({ headers });
	}

	public RegisterUser(token, user: User) {
		return this._http.post(GLOBAL.urlRegisterUser, user, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetUsers(token, page: number) {
		return this._http.get(GLOBAL.urlUsers + page, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetUsersSearch(token, page, object) {
		return this._http.post(GLOBAL.urlGetUsersSearch + page, object, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetUserBySearchAndRole(token, page: number, params) {
		return this._http.post(GLOBAL.urlGetUsersSearchRole + page, params, this.generateHeaders(token))
			.map(res => res.json());
	}

	public GetUserData(token, id: string) {
		return this._http.get(GLOBAL.urlGetUserData + id, this.generateHeaders(token))
			.map(res => res.json());
	}

	public UpdateUser(token, user: User, id: string) {
		return this._http.put(GLOBAL.urlUpdateUser + id, user, this.generateHeaders(token))
			.map(res => res.json());
	}

	public DeleteUser(token, id: string) {
		return this._http.delete(GLOBAL.urlDeleteUsers + id, this.generateHeaders(token))
			.map(res => res.json());
	}

	public signin(params, getHash = null) {
		if (getHash != null) {
			params.getHash = getHash;
		}


		const headers = new Headers({ 'Content-Type': 'application/json' });
		return this._http.post(GLOBAL.urlLoginWeb, params, { headers: headers })
			.map(res => res.json());
	}

	public forgotPassword(token, email) {
		return this._http.post(GLOBAL.urlForgotPassword, email, this.generateHeaders(token))
			.map(res => res.json());
	}

	public resetPassword(token , password) {
		return this._http.post(GLOBAL.urlResetPassword+token, password, this.generateHeaders(token))
			.map(res => res.json());
	}

	public verifyData(token, object) {
		return this._http.post(GLOBAL.urlVerifyData, object, this.generateHeaders(token))
			.map(res => res.json());
	}

	public verifyEmail(token, object) {
		return this._http.post(GLOBAL.urlVerifyEmail, object, this.generateHeaders(token))
			.map(res => res.json());
	}

	getIdentity():User {
		const identity: User = JSON.parse(localStorage.getItem('identity'));

		if (identity !== null ) {
			return identity;
		}
		else {
			this.identity = null;
		}

	}

	getToken(): string {
		const token = localStorage.getItem('token');
		if (token !== 'undefinided') {
			this.token = token;
		}
		else {
			this.token = null;
		}
		return this.token;
	}

}