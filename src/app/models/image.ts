export class Image {
    image: string;
    comment: string;
    user: string;
    imageAsset: any;
    stage: number;
}