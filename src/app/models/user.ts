export class User {
    _id:string;
    idEmployee:string;
    password: string;
    name: string;
    surname: string;
    fullname;
    email: string;
    workArea:string;
    workCharge: string;
    startDate: string;
    systemRole:string;
    ideaRole:string;
    phone:number;
    image:string;
    isSelected:boolean;
    confirmPassword:string;
    notifyUpdateIdea:boolean;
    notifyAddIde:boolean;
    info:any;
}