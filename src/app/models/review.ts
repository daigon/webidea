export class Review {
    user: String;
    teamFeel: Number;
    safety: Number;
    ergonomics: Number;
    customer: Number;
    score: Number;
}
