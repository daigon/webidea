import { notify } from './notify';
export class Idea {
    _id:string;
	title: string;
    image: Array<any>;
    problem: string;Date;
    description:string;
    result: string;
    experiment: string;
    verify: string;
    implementation:string;
    startdate: string;
    score:number;
    likes: number;
    status: number;
    isShow: boolean;
    author:string;
    coach: string;
    collaborator:Array<any>;
    comments: Array<any>;
    review: string;
    step1date: Date;
    step2date: Date;
    step3date: Date;
    step4date: Date;
    step5date: Date;
    notify: Array<notify>;
}