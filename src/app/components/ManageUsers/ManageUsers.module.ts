import { FooterModule } from './../../shared/components/footer/footer.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbPaginationModule, NgbDatepickerModule } from "@ng-bootstrap/ng-bootstrap";
import { UpdateUserComponent } from './updateUser/updateUser.component';
import { AddUserComponent } from './addUsers/addUser.component';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ManageUsersRoutingModule } from './ManageUsers-routing.module';
import { ManageUsersComponent } from './ManageUsers.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
    imports: [
        CommonModule,
        NgbDropdownModule.forRoot(),
        NgbPaginationModule.forRoot(),
        ManageUsersRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFontAwesomeModule,
        FooterModule,
        TextMaskModule,
        NgbDatepickerModule
    ],
    declarations: [
      ManageUsersComponent,
      AddUserComponent,
      UpdateUserComponent
    ]
})
export class ManageUsersModule { }
