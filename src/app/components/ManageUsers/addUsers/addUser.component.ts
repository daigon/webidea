import { Router } from '@angular/router';
import { GLOBAL } from './../../../services/global';
import {
  Attribute,
  Component,
  Directive,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALIDATORS, Validator, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { UploadService } from './../../../services/upload.service';

import { UserService } from './../../../services/user.service';
import { WendyConfiguartionService } from '../../../services/wendyConfiguartionService';
import { User } from './../../../models/user';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import { NgControlStatus } from '@angular/forms/src/directives/ng_control_status';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { isDate } from 'util';


@Component({
  selector: 'app-addUsers',
  templateUrl: './addUser.component.html',
  styleUrls: ['./addUser.component.scss'],

  providers: [UserService, UploadService, WendyConfiguartionService]
})

export class AddUserComponent implements OnInit {
  public isCoach: boolean = false;
  public isCollaborator: boolean = false;
  public urlGetImage = GLOBAL.urlGetImageUser;
  public user: User;
  public closeResult: string;

  public ideRole: Array<any> = [{ value: 'COLLABORATOR', show: 'Colaborador' }, { value: 'COACH', show: 'COACH' }];
  public workArea: Array<any> = [];
  public workCharge: Array<any> = [];
  public systemRole: Array<any> = [{ value: 'ROLE_USER', show: 'Publico' }, { value: 'ROLE_ADMIN', show: 'Administrador' }];

  public imageName;
  public avatar;
  public defaultImage;
  public messageId = "";
  public messageEmail = "";
  public isIdCorrect = true;
  public isEmailCorrect = true;
  mask: any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  modelDate: NgbDateStruct;
  constructor(private _userService: UserService,
    private modalService: NgbModal,
    private _uploadService: UploadService,
    private _router: Router,
    private _wendyConfiguartionService: WendyConfiguartionService
  ) {
    this.defaultImage = "assets/images/default.png";
    this.user = new User();
    this.fileToUpload[0] = null;


  }

  ngOnInit() {
  }
  /* ----------------------------------------------  Metodos para cargar datos de api -------------------------------------- */
  onSubmit() {
    this.verifyData();
    this.verifyEmail();
    if (this.validateData().length === 0) {
      this.user.fullname = this.user.name + ' ' + this.user.surname;

      this._userService.RegisterUser(this._userService.getToken(), this.user).subscribe(
        res => {
          if (this.fileToUpload[0] !== null) {
            this._uploadService.makeFileRequest([], this.fileToUpload, this._userService.getToken(), 'image', res._id, this.user.image)
              .then(
              (res: any) => {
                this.user = res.user;

                //localStorage.setItem('identity', JSON.stringify(this.user));
                //const image_path = GLOBAL.urlGetUserImage + this.user.image;
                //document.getElementById('image-logged').setAttribute('src', image_path);
                swal(
                  'Bien hecho',
                  'El usuario se ha agregado',
                  'success'
                )
                  .then(() => {
                    window.location.reload();
                  });
                this.user = new User();
              },
              (err) => {
                try {
                  var error = JSON.parse(err._body);

                  if (error['message'] !== undefined) {
                    swal("Error", error.message, "error")
                  }
                  else if (error['isCorrect'] !== null) {
                    console.log("here")
                    if (!error.isCorrect) {

                      swal({ 
                      title: "Sesión expirada", 
                      text: "Tu sesión a expirado, debes de volver a iniciar sesión",
                      allowOutsideClick: false,  
                      imageUrl: 'assets/icons/success.png',
                      imageAlt: 'success',
                      confirmButtonClass: 'btn btnAdd',
                      confirmButtonText: 'Aceptar'
                      }
                     )
                        .then(() => {
                          localStorage.clear();
                          this._router.navigate(['/login']);
                          window.location.reload()
                        });


                    }
                  }
                } catch (error) {
                  swal(
                    { 
                      title: "Error", 
                      text: "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.",
                      allowOutsideClick: false,  
                      imageUrl: 'assets/icons/waring.png',
                      imageAlt: 'success',
                      confirmButtonClass: 'btn btnAdd',
                      confirmButtonText: 'Aceptar'
                      }
                    );
                }
              }
              );

          }
          else {
            swal(
              { 
                title: "Bien hecho", 
                text: "El usuario se ha creado",
                allowOutsideClick: false,  
                imageUrl: 'assets/icons/success.png',
                imageAlt: 'success',
                confirmButtonClass: 'btn btnAdd',
                confirmButtonText: 'Aceptar'
                }
              
            )
              .then(() => {
                window.location.reload();
              });

          }

        },
        err => {
          swal('Error', err._body.message, 'error');
          if (err._body.message !== undefined) {
            try {
              var error = JSON.parse(err._body);
              console.log(error['isCorrect'] !== null);
              if (error['isCorrect'] !== null) {
                console.log("here")
                if (!error.isCorrect) {
                  swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                    .then(() => {
                      localStorage.clear();
                      this._router.navigate(['/login']);
                      window.location.reload()
                    });


                }
              }
            } catch (error) {
              swal("Error", "Error en el servidor", "error");
            }
          }

        }
      );
    }
    else {
      swal(
        'Error',
        this.validateData(),
        'error'
      )

    }

  }

  /* ----------------------------------------------  Metodos para validar datos de formulario -------------------------------------- */
  validateData(): string {

    if (this.isCoach && this.isCollaborator) {
      this.user.ideaRole = 'BOTH';
    }
    else if (this.isCoach) {
      this.user.ideaRole = 'COACH';
    }
    else if (this.isCollaborator) {
      this.user.ideaRole = 'COLLABORATOR';
    }

    if (this.modelDate) {
      this.user.startDate = this.modelDate.year + '-' + this.modelDate.month + '-' + this.modelDate.day;
    }
    if (!this.isIdCorrect) {
      return "El id que ingresaste no esta disponible";
    }
    if (!this.isEmailCorrect) {
      return "El email que ingresaste no esta disponible";
    }

    if (this.user.idEmployee === undefined || this.user.idEmployee.length === 0) {
      return "Debes de agregar el id del empleado";
    }
    else if (this.user.password === undefined) {
      return "Debes de agregar una contraseña";
    }
    else if (this.user.password.length < 8) {
      return "La contraseña debe contener al menos 8 caracteres";
    }
    else if (this.user.confirmPassword !== this.user.password) {
      return "Las contraseñas no coinciden";
    }
    else if (this.user.name === undefined || this.user.name.length === 0) {
      return "Debes de agregar el nombre del empleado";
    }
    else if (this.user.surname === undefined || this.user.surname.length === 0) {
      return "Debes de agregar el apellido del empleado";
    }
    // else if (this.user.email === undefined || this.user.email.length === 0) {
    //   return "Debes de agregar un email";
    // }
    else if (this.user.systemRole === undefined || this.user.systemRole.length === 0) {
      return "Debes de elegir el permiso que tendra el usuario para usar el sistema"
    }
    else if (this.user.phone === undefined || this.user.phone === null) {
      return "Debes de agregar el telefono del empleado"
    }
    else if (this.user.startDate === undefined || this.user.startDate.length === 0
      || this.modelDate.year === undefined || this.modelDate.month === undefined || this.modelDate.day === undefined) {
      return "Debes de ingresar la fecha en la cual el empleado inicio su trabajo";
    }
    else if (this.user.workArea === undefined || this.user.workArea.length === 0) {
      return "Debes de elegir el area de trabajo del empleado";
    }
    else if (this.user.workCharge === undefined || this.user.workCharge.length === 0) {
      return "Debes de elegir el puesto del empleado";
    }
    else if (this.user.ideaRole === undefined || this.user.ideaRole.length === 0) {
      return "Debes de elegir el rol que tendra el empleado para en las ideas que se generaran"
    }
    else {
      return "";
    }

  }

  public fileToUpload: Array<File> = [];

  filechangeEvent(fileInut: any) {

    if (fileInut.target.files.length > 0) {
      this.fileToUpload = <Array<File>>fileInut.target.files;
      this.imageName = this.fileToUpload[0].name;

      let reader = new FileReader();

      reader.onload = (e: any) => {

        this.avatar = e.target.result;
      }

      console.log(this.fileToUpload);
      reader.readAsDataURL(this.fileToUpload[0]);
    }


  }

  /* ----------------------------------------------  Metodos Modal -------------------------------------- */

  public open(content) {
    this.getWorks();
    this.isEmailCorrect = true;
    this.isIdCorrect = true;
    this.messageEmail = "";
    this.messageId = "";
    this.modalService.open(content, { windowClass: 'dark-modal', size: "lg", });

  }

  public resetData() {
    this.avatar = null;
    this.user = new User();
  }

  public verifyData(): boolean {
    this._userService.verifyData(this._userService.getToken(), this.user).subscribe(
      res => {
        if (res.message.id !== undefined) {
          this.messageId = res.message.id

          if (res.message.value === 0) {
            this.isIdCorrect = true;
            return true;
          }
          else {
            this.isIdCorrect = false;
            return false;
          }
        }
      },
      err => {

        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {
            if (error.message.id != undefined) {
              this.messageId = error.message.id;
              //swal("Error", error.message.id, "error")
            }

          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                });


            }
          }
        } catch (error) {
          console.log(error);
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
        return false;
      }
    );
    return false;
  }

  public verifyEmail(): boolean {
    this._userService.verifyEmail(this._userService.getToken(), this.user).subscribe(
      res => {
        if (res.message.email !== undefined) {
          this.messageEmail = res.message.email;

          if (res.message.value === 0) {
            this.isEmailCorrect = true;
            return true;
          }
          else {
            this.isEmailCorrect = false;
            return false;
          }
        }

      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {
            if (error.message.email != undefined) {
              //swal("Error", error.message.email, "error")
              this.messageEmail = error.message.email;
            }
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }


      }
    );
    return false;
  }

  public verifyCoach() {
    if (this.isCoach) {
      this.isCoach = false;
    }
    else {
      this.isCoach = true;
    }

  }

  public verifyCollaborator() {
    if (this.isCollaborator) {
      this.isCollaborator = false;
    }
    else {
      this.isCollaborator = true;
    }
  }

  // -------------------------------  Metodo ABC para Works --------------------------------- //

  public addWork(wor: string, addA: boolean) {
    var flag: boolean;
    if (addA) {
      var work: any = { iswork: true, area: wor };
      for (let i = 0; i < this.workArea.length; i++) {
        if (work.area === this.workArea[i]) {
          flag = true;
          break;
        } else {
          flag = false;
        }
      }
      if (flag) {
        swal.showValidationError(
          'El Area Ya Esta Registrada.'
        );
      } else {
        this._wendyConfiguartionService.AddWorks(this._userService.getToken(), work)
          .subscribe(
          addWork => {

            this.workArea = addWork.workArea;
            this.user.workArea = "";
            swal(
              'Agregada',
              'El Área ha sido actualizada con exito!',
              'success'
            );
          },
          (error) => {
            try {
              var error = JSON.parse(error._body);
              console.log(error.message);
            } catch (err) {
              console.log(error);
            }

          });
      }

    } else {
      var work: any = { iswork: false, charge: wor };
      for (let i = 0; i < this.workCharge.length; i++) {
        if (work.charge === this.workCharge[i]) {
          flag = true;
          break;
        } else {
          flag = false;
        }
      }
      if (flag) {
        swal.showValidationError(
          'El Area Ya Esta Registrada.'
        );
      } else {
        this._wendyConfiguartionService.AddWorks(this._userService.getToken(), work)
          .subscribe(
          addWork => {

            this.workCharge = addWork.workCharge;
            this.user.workCharge = "";
            swal(
              'Agregado',
              'El puesto ha sido actualizado con exito!',
              'success'
            );
          },
          (error) => {
            try {
              var error = JSON.parse(error._body);
              console.log(error.message);
            } catch (err) {
              console.log(error);
            }
          });

      }
    }
  }

  public updateWork(upWor: string, updaA: boolean) {
    var flagUpdate: boolean;
    if (updaA) {
      var work: any = { iswork: true, areanew: upWor, areaold: this.user.workArea };
    } else {
      var work: any = { iswork: false, chargenew: upWor, chargeold: this.user.workCharge };
    }
    this._wendyConfiguartionService.UpdateWorks(this._userService.getToken(), work)
      .subscribe(
      updateWork => {
        if (updaA) {

          this.workArea = updateWork.workArea;
          for (let i = 0; i < this.workArea.length; i++) {
            console.log(work.areanew === this.workArea[i]);
            if (work.areanew === this.workArea[i]) {
              flagUpdate = true;
              this.user.workArea = this.workArea[i];
              break;
            } else {
              flagUpdate = false;
            }
          }
          if (flagUpdate) {
            swal(
              'Actualizada',
              'El area ha sido actualizada con exito!',
              'success'
            );
          } else {
            swal.showValidationError(
              'No puedes actualizar un dato que no esta registrado'

            );
          }
        } else {
          this.workCharge = updateWork.workCharge;
          for (let i = 0; i < this.workCharge.length; i++) {
            console.log(work.chargenew === this.workCharge[i]);
            if (work.chargenew === this.workCharge[i]) {
              flagUpdate = true;
              this.user.workCharge = this.workCharge[i];
              break;
            } else {
              flagUpdate = false;
            }
          }
          if (flagUpdate) {
            swal(
              'Actualizado',
              'El puesto ha sido actualizado con exito!',
              'success'
            );
          } else {
            swal.showValidationError(
              'No puedes actualizar un dato que no esta registrado'

            );
          }
        }
      },
      (error) => {
        try {
          var error = JSON.parse(error._body);
          console.log(error.message);
        } catch (err) {
          console.log(error);
        }
      });
  }


  public getWorks() {
    this._wendyConfiguartionService.GetWorks(this._userService.getToken())
      .subscribe(
      getwork => {
        this.workArea = getwork.workArea;
        this.workCharge = getwork.workCharge;
      },
      (error) => {
        console.log(error.message);
      });
  }
  // ------------------------------- sweet Alert Add --------------------------------------- //
  public alertAdd(aleA: boolean) {
    swal({
      title: aleA ? 'Agrega una nueva area de trabajo' : 'Agrega un nuevo  puesto de trabajo',
      input: 'text',
      inputValidator: result => new Promise<void>((resolve, reject) => {
        result ? resolve() : reject('No puedes dejar el campo vacio!');
      }),
      confirmButtonText: 'Agregar',
      showCancelButton: true,

      preConfirm: (addWork) => { // recordatorio estamos utilizando TS y no JS;
        return new Promise((resolve) => {
          if (aleA) {
            this.addWork(addWork, true);

          } else {
            this.addWork(addWork, false);
          }
        });
      },
      // allowOutsideClick: false
    }).then(function (result) {

    },
      function (dismiss) {
        console.log(dismiss);
      });
  }

  public alertUpdate(aleUpA: boolean) {

    swal({
      title: aleUpA ? 'Actualiza el area de trabajo' : 'Actualiza el puesto de trabajo',
      input: 'text',
      inputPlaceholder: aleUpA ? 'Primero debes seleccionar un area de trabajo' : 'Primero debes seleccionar un puest de trabajo',
      inputValue: aleUpA ? this.user.workArea === undefined ? '' : this.user.workArea : this.user.workCharge === undefined ? ''
        : this.user.workCharge,
      confirmButtonText: 'Actualizar',
      showCancelButton: true,
      inputValidator: result => new Promise<void>((resolve, reject) => {
        result ? resolve() : reject('No puedes dejar el campo vacio!');
      }),
      preConfirm: (upWork) => { // recordatorio estamos utilizando TS y no JS;
        return new Promise((resolve) => {
          setTimeout(() => {
            if (aleUpA) {
              for (let i = 0; i < this.workArea.length; i++) {

                if (this.workArea[i] !== upWork) {
                  this.updateWork(upWork, true);

                }

              }
            } else {
              for (let i = 0; i < this.workCharge.length; i++) {
                if (upWork !== this.workCharge[i]) {

                  this.updateWork(upWork, false);

                } else {
                  swal.showValidationError(
                    'El Puesto Ya Esta Registrado.'
                  );
                }
              }
            }
          }, );
        });
      },
      // allowOutsideClick: false
    }).then(function (result) {
    },
      function (dismiss) {
        console.log(dismiss);
      });
  }

  public alertDelete(aleDel: boolean) {
    if (aleDel) {
      var work: any = { iswork: true, area: this.user.workArea };
    } else {
      var work: any = { iswork: false, charge: this.user.workCharge };
    }
    swal({
      title: '¿Estas seguro?',
      text: aleDel ? 'Las areas borradas no se podran recuperar' : 'Los puestos borrados no se podran recuperar',
      showCancelButton: true,
      confirmButtonClass: 'btn btnAdd',
      cancelButtonClass: 'btn btn-light',
      confirmButtonText: 'Aceptar',
      imageUrl: 'assets/icons/waring.png',
      imageAlt: 'waring'
    })
      .then(() => {
        this._wendyConfiguartionService.DeleteWorks(this._userService.getToken(), work)
          .subscribe(
          deleteWork => {
            var flagDel: boolean;
            if (aleDel) {
              for (let i = 0; i < this.workArea.length; i++) {
                if (work.area === this.workArea[i]) {
                  flagDel = true;
                  break;
                } else {
                  flagDel = false;
                }
              }
              if (!flagDel) {
                this.workArea = deleteWork.workArea;
                this.user.workArea = "";
              } else {
                this.workArea[0] = null;
                this.workArea = deleteWork.workArea;
                this.user.workArea = "";
              }
            } else {
              for (let i = 0; i < this.workCharge.length; i++) {
                if (work.charge === this.workCharge[i]) {
                  flagDel = true;
                  break;
                } else {
                  flagDel = false;
                }
              }
              if (!flagDel) {
                this.workCharge = deleteWork.workCharge;
                this.user.workCharge = "";
              } else {
                this.workCharge[0] = null;
                this.workCharge = deleteWork.workCharge;
                this.user.workCharge = "";
              }
            }
            swal(
              'Borrado',
              aleDel ? 'El Área ha sido borrada con exito!' : 'El puesto ha sido borrado con exito!',
              'success'
            );

          },
          (error) => {
            try {
              var error = JSON.parse(error._body);
              console.log(error.message);
            } catch (err) {
              console.log(error);
            }
          }
          );

      },
      function (dismiss) {
        console.log(dismiss);
      });
  }
}