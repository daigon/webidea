import { User } from './../../models/user';
import { UpdateUserComponent } from './updateUser/updateUser.component';
import { AddUserComponent } from './addUsers/addUser.component';
import { UserService } from './../../services/user.service';
import { IdeaService } from './../../services/Idea.service';
import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Subscription } from 'rxjs/Subscription';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-ManageUsers',
  templateUrl: './ManageUsers.component.html',
  styleUrls: ['./ManageUsers.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [IdeaService],
  entryComponents: [AddUserComponent, UpdateUserComponent],
  animations: [routerTransition()]
})


export class ManageUsersComponent implements OnInit, OnDestroy {
  @ViewChild(AddUserComponent)
  private modalAdd: AddUserComponent;
  @ViewChild(UpdateUserComponent)
  private modalUpdate: UpdateUserComponent;


  public users: Array<User>;
  public listUsers: Array<User>;

  subscription: Subscription;
  public search: string;
  public paginationType: string;
  itemsPerPage: number;
  totalItems: any;
  page: any;
  previousPage: any;
  public Role;

  constructor(
    private router: Router,
    private _ideaService: IdeaService,
    private _userService: UserService,
    private _router: Router
  ) {
    this.page = 1;
    this.getUsers(1);

  }

  ngOnInit() {

  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks

  }
  /* ----------------------------------------------- cargar datos de api-------------------------------- */
  public getUsers(page: number) {

    this._userService.GetUsers(this._userService.getToken(), page).subscribe(
      res => {

        this.users = res.users;


        this.totalItems = res.pages;
        this.page = page;
        this.listUsers = res.users;
        this.itemsPerPage = this.listUsers.length;
        this.paginationType = "users";
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);

                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }

      }
    );
  }

  getCollaborators(page: number) {
    this._ideaService.GetCollaborators(this._userService.getToken(), page).subscribe(
      res => {

        this.users = res.collaborators;

        this.totalItems = res.pages;
        this.page = page;

        this.listUsers = res.collaborators;
        this.itemsPerPage = this.listUsers.length;
        this.paginationType = "collaborators";
        this.Role = "COLLABORATOR";

        this.listUsers = this.listUsers.sort((n1, n2) => {
          if (n1.name.toUpperCase() > n2.name.toUpperCase()) {
            return 1;
          }
          if (n1.name.toUpperCase() < n2.name.toUpperCase()) {
            return -1;
          }
          return 0;
        });

      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);

                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }
    );
  }

  getCoach(page: number) {
    this._ideaService.GetCoach(this._userService.getToken(), page).subscribe(
      res => {
        this.users = res.coaches;
        this.listUsers = res.coaches;

        this.totalItems = res.pages;
        this.page = page;

        this.paginationType = "coaches";
        this.itemsPerPage = this.listUsers.length;
        this.Role = "COACH";
        this.listUsers = this.listUsers.sort((n1, n2) => {
          if (n1.name.toUpperCase() > n2.name.toUpperCase()) {
            return 1;
          }
          if (n1.name.toUpperCase() < n2.name.toUpperCase()) {
            return -1;
          }
          return 0;
        });
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);

                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }
    );
  }

  getUsersSearch(criteria, page: number) {
    var body = { criteria: criteria };
    if (page.valueOf() === NaN)
      page = 1;
    this._userService.GetUsersSearch(this._userService.getToken(), page, body).subscribe(
      res => {
        this.users = res.users;


        this.totalItems = res.pages;
        this.page = page;
        this.listUsers = res.users;
        this.itemsPerPage = this.listUsers.length;
        this.paginationType = "search";


      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);

                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }
    );

  }
  getUsersSearchAndRole(criteria, page: number) {
    var body = { criteria: criteria, role: this.Role };
    if (page.valueOf() === NaN)
      page = 1;
    this._userService.GetUserBySearchAndRole(this._userService.getToken(), page, body).subscribe(
      res => {
        this.users = res.users;


        this.totalItems = res.pages;
        this.page = page;
        this.listUsers = res.users;
        this.itemsPerPage = this.listUsers.length;
        this.paginationType = "searchandrole";
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {
            console.log("here")
            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);

                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }
    );

  }

  /** ------------------------------------ Metodo para filtrar datos ------------------------ */

  filteredBy(args) {

    if (args.target.value == 1) {
      this.getCollaborators(1);
    }
    else if (args.target.value == 2) {
      this.getCoach(1);
    }
    else {
      this.getUsers(1)
    }

  }

  public onSearch(args) {

    this.search;

    this.listUsers = new Array();
    if (this.search !== "") {
      if (this.Role === "COACH" || this.Role === "COLLABORATOR")
        this.getUsersSearchAndRole(this.search, 1)
      else
        this.getUsersSearch(this.search, this.page);
    }
    else {
      this.onClear(args);
    }
  }

  public onClear(args) {

    args.target.text = "";
    args.target.placeholder = "Buscar";
    if (this.Role === "COACH") {
      this.getCoach(1);
    }
    else if (this.Role === "COLLABORATOR") {
      this.getCollaborators(1);
    }
    else {
      this.getUsers(1);
    }


  }

  /* ----------------------------------------------- Acciones -------------------------------- */
  Delete(_id) {
    swal({
      title: '¿Estas seguro que quieres borrar al usuario?',
      text: "Los usuario borrados no se podran recuperar",
      showCancelButton: true,
      confirmButtonClass: 'btn btnAdd',
      cancelButtonClass: 'btn btn-light',
      confirmButtonText: 'Aceptar',
      imageUrl: 'assets/icons/waring.png',
      imageAlt: 'waring'
    })
      .then(deleteUser => {

        if (deleteUser) {
          this._userService.DeleteUser(this._userService.getToken(), _id).subscribe(
            res => {
              swal(
                {
                  title: 'Borrado',
                  text: "El suario ha sido borrado con exito!",
                  confirmButtonClass: 'btn btnAdd',
                  imageUrl: 'assets/icons/succes.png',

                  imageAlt: 'Custom image'
                }
              );

              this.loadPage(this.page)
            },
            err => {
              console.log(err)
              try {
                var error = JSON.parse(err._body);

                if (error['message'] !== undefined) {

                  swal("Error", error.message, "error")
                }
                else if (error['isCorrect'] !== null) {
                  console.log("here")
                  if (!error.isCorrect) {
                    swal({
                      title: "Sesión expirada",
                      text: "Tu sesión a expirado, debes de volver a iniciar sesión",
                      imageUrl: 'assets/icons/waring.png',
                      imageAlt: 'Custom image',
                      allowOutsideClick: false
                    }
                    )
                      .then(() => {
                        localStorage.clear();
                        this._router.navigate(['/login']);

                        this._userService.isLogged = false
                      });


                  }
                }
              } catch (error) {
                swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
              }
            }
          );
        }
      });
  }

  /** ------------------------------------- Paginacion ----------------------------------------- */
  loadPage(page: number) {
    if (this.paginationType === "users") {
      this.getUsers(page);

    }
    else if (this.paginationType === "collaborators") {
      this.getCollaborators(page);

    }
    else if (this.paginationType === "coaches") {
      this.getCoach(page);

    }
    else if (this.paginationType === "search") {
      this.getUsersSearch(this.search, page)

    }
    else if (this.paginationType === "searchandrole") {
      this.getUsersSearchAndRole(this.search, page)

    }

  }

}
