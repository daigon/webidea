import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { notify } from './../../models/notify';
import { Idea } from './../../models/Idea';
import { UserService } from './../../services/user.service';

import { Image } from './../../models/image';
import { IdeaService } from './../../services/Idea.service';
import { Component, OnInit, HostListener, ViewChild } from '@angular/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import { GLOBAL } from '../../services/global';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { error } from 'selenium-webdriver';
import { User } from '../../models/user';


@Component({
  selector: 'app-ShowIdeas',
  templateUrl: './ShowIdeas.component.html',
  styleUrls: ['./ShowIdeas.component.scss'],
  providers: [
    IdeaService,
    NgbCarouselConfig,
    UserService]


})
export class ShowIdeasComponent implements OnInit {

  public author: User;
  public coach: User;
  public collaborators: Array<any> = [];
  public idea: Array<any> = [];
  public it: Array<any> = [];

  public faces: Array<any> = ['assets/icons/muy-mal.png', 'assets/icons/mal.png', 'assets/icons/normal.png', 'assets/icons/bien.png', 'assets/icons/excelente.png'];
  public faces2: Array<any> = ['assets/icons/mal.png', 'assets/icons/normal.png', 'assets/icons/bien.png'];
  public stats: Array<any> = ['Muy Mal', 'Mal', 'Normal', 'Bien', 'Excelente'];
  public stats2: Array<any> = ['Mal', 'Normal', 'Bien'];

  public ideas: Array<any> = [];
  public items: Array<any> = [];

  public ideaTitle: Array<any> = [];
  public itemTitle: Array<any> = [];

  public ideaAuthor: Array<any> = [];
  public itemAuthor: Array<any> = [];

  public ideaCoach: Array<any> = [];
  public itemCoach: Array<any> = [];

  public ideaProblem: Array<any> = [];
  public itemProblem: Array<any> = [];

  public ideaDescription: Array<any> = [];
  public itemDescription: Array<any> = [];

  public ideaResult: Array<any> = [];
  public itemResult: Array<any> = [];

  public ideaExperiment: Array<any> = [];
  public itemExperiment: Array<any> = [];

  public ideaVerify: Array<any> = [];
  public itemVerify: Array<any> = [];

  public ideaImplementation: Array<any> = [];
  public itemImplementation: Array<any> = [];

  public ideaImage: Array<any> = [];
  public itemImage: Array<any> = [];

  public arrayImageStatus0: Array<any> = [];
  public arrayImageStatus1: Array<any> = [];
  public arrayImageStatus2: Array<any> = [];
  public arrayImageStatus3: Array<any> = [];

  public notifyIdea: Idea;

  public arrayComments: Array<any> = [];

  public review: any;
  public search: string;

  public pageSize: any;
  public page: any;
  public pageCollection: number;

  public paginationType: string;

  public active;
  public buttonAction = 1;

  public urlGetImage;
  public urlImageUser = GLOBAL.urlGetImageUser;
  public showBigTabs = true;
  closeResult: string;

  iconLike = String.fromCharCode(0x906);
  iconComment = String.fromCharCode(0x973);

  public showCycle: boolean = false;
  constructor(
    private _IdeaService: IdeaService,
    private modalService: NgbModal,
    private _userService: UserService,
    config: NgbCarouselConfig,

    private _router: Router,
    configAccordion: NgbAccordionConfig) {
    // customize default values of accordions used by this component tree
    configAccordion.closeOthers = true;
    configAccordion.type = 'info';
    this.urlGetImage = GLOBAL.urlGetIdeaImage;
    this.GetIdeasByAuthor(1);
    this.active = 1;
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;

    this.author = new User();
    this.coach = new User();
  }

  public innerWidth: any;
  ngOnInit() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 992) {
      this.showBigTabs = true;
    }
    else {
      this.showBigTabs = false;
    }


  }

  public infoEmployee: User = new User();
  public workCharge = '';

  @ViewChild('p') public popover: NgbPopover;

  public changeGreeting(greeting: any): void {
    const isOpen = this.popover.isOpen();

    if (greeting !== this.workCharge || !isOpen) {
      this.infoEmployee = greeting;

    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 992) {
      this.showBigTabs = true;
    }
    else {
      this.showBigTabs = false;
    }

  }

  // --------------------------------------------metodos para cargar vistas ----------------------------------------------------------
  GetIdeasByScore(page) {

    this._IdeaService.GetIdeasByScore(this._userService.getToken(), page).subscribe(
      res => {
        this.items = null;
        this.ideas = res.ideas;
        this.items = this.ideas;
        this.page = page;
        this.pageCollection = res.pages;
        this.pageSize = this.items.length;

        this.paginationType = "score"
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }


    );
  }
  GetIdeasByAuthor(page) {
    var object = {
      page: page
    };

    this._IdeaService.GetIdeasByAuthor(this._userService.getIdentity()._id, object, this._userService.getToken()).subscribe(
      res => {
        this.items = null;
        this.ideas = res.ideas;
        this.items = this.ideas;
        this.page = page;
        this.pageCollection = res.pages;
        this.pageSize = this.items.length;

        this.paginationType = "author"
      },
      err => {

        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }


    );

  }
  GetIdeasByLikes(page: number) {
    this._IdeaService.GetIdeasByLikes(this._userService.getToken(), page).subscribe(
      res => {

        this.items = null;
        this.ideas = res.ideas;
        this.items = this.ideas;
        this.page = page;
        this.pageCollection = res.pages;
        this.pageSize = this.items.length;

        this.paginationType = "likes"
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }


    );
  }

  GetIdeasBySearch(page) {
    var object = {
      criteria: this.search
    };

    this._IdeaService.GetIdeasBySearch(page, object, this._userService.getToken()).subscribe(
      res => {
        this.items = null;
        this.ideas = res.ideas;
        this.items = this.ideas;
        this.page = page;
        this.pageCollection = res.pages;
        this.pageSize = this.items.length;

        this.paginationType = "search"
      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }


    );
  }

  GetIdeasBySearchAndAuthor(page) {
    var object = {
      page,
      criteria: this.search
    }

    this._IdeaService.GetIdeasBySearchAndAuthor(object, this._userService.getIdentity()._id, this._userService.getToken()).subscribe(
      res => {
        this.items = null;
        this.ideas = res.ideas;
        this.items = this.ideas;
        this.page = page;
        this.pageCollection = res.pages;
        this.pageSize = this.items.length;


      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });


            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }


    );
  }

  // --------------------------------------------------------metodo para filtrar ----------------------------------------------------------
  filterBy(arg) {

    if (arg.target.value == 1) {
      this.GetIdeasByScore(1);
    }
    if (arg.target.value == 2) {
      this.GetIdeasByAuthor(1);
    }
    if (arg.target.value == 3) {
      this.GetIdeasByLikes(1);
    }
  }


  // -------------------------------------------------------metodos para buscar--------------------------------------------------------
  public onSearch(args) {


    if (this.paginationType !== "author") {
      if (this.search !== "") {
        this.GetIdeasBySearch(this.page);

      }
    } else if (this.paginationType === "author") {
      this.GetIdeasBySearchAndAuthor(this.page);

    } else {
      this.onClear(args);
    }

  }

  public onClear(args) {
    args.text = "";
    args.placeholder = "Buscar";

    this.items = new Array();
    this.ideas.forEach(item => {
      this.items.push(item);
    });
  }

  loadPage(page: number) {
    if (this.paginationType === "score") {
      this.GetIdeasByScore(page);
    } else if (this.paginationType === "author") {
      this.GetIdeasByAuthor(page);
    } else if (this.paginationType === "likes") {
      this.GetIdeasByLikes(page);
    } else if (this.paginationType === "search") {
      this.GetIdeasBySearch(page);
    }
  }
  // ***************************************************estilos para agregar imagen de fondo *****************************************
  Styles(image) {
    //console.dir(image.image)
    if (image.image[0] === undefined || image.image[0] === null) {
      return {
        'background-image': 'url(\"' + '/assets/icons/icon-img-tmp.png' + '\")'
      };
    }

    if (image.image.length > 0) {
      return {
        'background-image': 'url(\"' + this.urlGetImage + image.image[0].image + '\")'

      };
    }
  }


  // --------------------------------------------------------abrir modal -----------------------------.----------------------------------

  openIdea(id, idea) {
    this.showCycle = true;
    window.scrollTo(0, 0)
    this.active = 1;
    this.GetIdea(id);

  }

  closeIdea() {
    this.showCycle = false;
    window.scrollTo(0, 0)
  }

  IdeaImage(image, number) {

    if (image.length >= 1 && image.length == number + 1) {
      return this.urlGetImage + image[number].image;
    } else if (image[0] !== undefined) {
      return this.urlGetImage + image[0].image;
    } else {
      return undefined;
    }
  }

  GetIdea(id) {
    this.arrayImageStatus0 = [];
    this.arrayImageStatus1 = [];
    this.arrayImageStatus2 = [];
    this.arrayImageStatus3 = [];
    this._IdeaService.GetIdea(this._userService.getToken(), id).subscribe(
      res => {
        this.it = null;
        this.idea = res.collaborator;
        this.it = this.idea;
        this.notifyIdea = res;

        this.notify(this.notifyIdea);

        this.ideaTitle = res.title;
        this.itemTitle = this.ideaTitle;

        this.ideaAuthor = res.author.image;
        this.author = res.author;

        this.coach = res.coach;
        this.collaborators = res.collaborator;

        this.itemAuthor = this.ideaAuthor;

        this.ideaCoach = res.coach.image;
        this.itemCoach = this.ideaCoach;

        this.ideaProblem = res.problem;
        this.itemProblem = this.ideaProblem;

        this.ideaDescription = res.description;
        this.itemDescription = this.ideaDescription;

        this.ideaResult = res.result;
        this.itemResult = this.ideaResult;

        this.ideaExperiment = res.experiment;
        this.itemExperiment = this.ideaExperiment;

        this.ideaVerify = res.verify;
        this.itemVerify = this.ideaVerify;

        this.ideaImplementation = res.implementation;
        this.itemImplementation = this.ideaImplementation;

        this.ideaImage = res.image;
        this.itemImage = this.ideaImage;

        this.arrayComments = res.comments;

        for (let i = 0; i < this.itemImage.length; i++) {
          if (this.itemImage[i].stage == 0) {
            this.arrayImageStatus0.push(this.itemImage[i]);
          } else if (this.itemImage[i].stage == 1) {
            this.arrayImageStatus1.push(this.itemImage[i]);
          } else if (this.itemImage[i].stage == 2) {
            this.arrayImageStatus2.push(this.itemImage[i]);
          } else if (this.itemImage[i].stage == 3) {
            this.arrayImageStatus3.push(this.itemImage[i]);
          }
        }

        this.review = res.review;

      },
      err => {
        try {
          var error = JSON.parse(err._body);

          if (error['message'] !== undefined) {

            swal("Error", error.message, "error")
          }
          else if (error['isCorrect'] !== null) {

            if (!error.isCorrect) {
              swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                .then(() => {
                  localStorage.clear();
                  this._router.navigate(['/login']);
                  window.location.reload();
                  this._userService.isLogged = false
                });
            }
          }
        } catch (error) {
          swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
        }
      }
    );
  }

  errorHandlerIdeaImage(event,id) {
    
    // event.srcElement.src = '/assets/icons/icon-img-tmp.png'
    document.getElementById(id).setAttribute('src', '/assets/icons/placeholder.png');
  }

  errorHandlerAuthorImage(event,id) {
    
    // event.srcElement.src = '/assets/images/notfound.png'
    document.getElementById(id).setAttribute('src', '/assets/icons/userDefault.png');
  }

  errorHandlerAuthorImageWhite(event,id) {
    
    // event.srcElement.src = '/assets/images/notfound.png'
    document.getElementById(id).setAttribute('src', '/assets/icons/icon-user.png');
  }

  activeDiv(number: number) {
    this.active = number;
  }
  /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> modal comentarios <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< */

  openComments(content, id) {
    this.active = 1;
    this.GetIdea(id);
    this.modalService.open(content);
  }

  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>actualizar estado de la notificacion <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  notify(idea) {
    idea.notify.forEach(n => {
      if (n.user == this._userService.identity._id) {
        this._IdeaService.UpdateNotify(this._userService.getToken(), n, idea._id).subscribe(
          res => {
          },
          err => {
            try {
              var error = JSON.parse(err._body);
              if (error['message'] !== undefined) {
                swal("Error", error.message, "error")
              }
              else if (error['isCorrect'] !== null) {
                if (!error.isCorrect) {
                  swal({ title: "Sesión expirada", text: "Tu sesión a expirado, debes de volver a iniciar sesión", type: "info", allowOutsideClick: false })
                    .then(() => {
                      localStorage.clear();
                      this._router.navigate(['/login']);
                      window.location.reload();
                      this._userService.isLogged = false
                    });
                }
              }
            } catch (error) {
              swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
            }
          }
        );
      }

    });

  }
  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>mostrar notificacion <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  verifyNotify(idea): Boolean {
    var state;
    idea.notify.forEach(n => {
      if (n.user == this._userService.identity._id) {
        if (n.notify == true) {
          state = true;
        }
        else {
          state = false;
        }
      }
      else {
        state = false;
      }
    });
    return state;
  }
  ideaButtons(button) {
    if (button == 0) {
      this.buttonAction = 1;
    }
    if (button == 1) {
      this.buttonAction = 2;
    }
  }
  public ESCAPE_KEYCODE = 27;
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.keyCode === this.ESCAPE_KEYCODE) {
      this.closeIdea();
    }
  }
}
