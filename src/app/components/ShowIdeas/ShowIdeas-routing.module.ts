import { ShowIdeasComponent } from './ShowIdeas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: ShowIdeasComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShowIdeasRoutingModule { }

export const routedComponents = [ShowIdeasComponent];