import { FooterModule } from './../../shared/components/footer/footer.module';

import { CommonModule } from '@angular/common';
import { IdeaService } from './../../services/Idea.service';
import { ShowIdeasRoutingModule } from './ShowIdeas-routing.module';
import { FormsModule } from '@angular/forms';
import { ShowIdeasComponent } from './ShowIdeas.component';
import { NgModule } from '@angular/core';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { NgbCarouselModule,NgbTabsetModule,NgbAccordionModule,NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';




@NgModule({
    imports: [
        FormsModule,
        ShowIdeasRoutingModule,
        CommonModule,
        NgbPaginationModule.forRoot(),
        AngularFontAwesomeModule,
        NgbCarouselModule,
        NgbTabsetModule,
        NgbAccordionModule,
        FooterModule,
        NgbPopoverModule
    ],
    exports: [],
    declarations: [ShowIdeasComponent],
    providers: [IdeaService],
})
export class ShowIdeasModule { }
