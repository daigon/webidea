import { Router, Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { UserService } from './../../services/user.service';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
@Component({
  selector: 'app-ResetPassword',
  templateUrl: './ResetPassword.component.html',
  styleUrls: ['./ResetPassword.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  public user:User;
  private token;
  constructor(
    private _userService:UserService,
    private _router:Router,
    private _route:ActivatedRoute
  ) {
    this.user = new User();
   }

  ngOnInit() {
    this._route.params.forEach((params) => {
      this.token = params['token'];
    });
  }

  onSubmit() {
    if(this.validateData() === 1){
     var password = {password:this.user.password};
      this._userService.resetPassword(this.token,password).subscribe(
        res => {
          swal("Contraseña cambiada",res.message,"success");
          this._router.navigate(['/login']);
        },
        err => {
          try {
            var error = JSON.parse(err._body);
            
            if(error['message'] !== undefined){
              
              swal("Error", error.message, "error")
            }
            else if (error['isCorrect'] !== null) {
              console.log("here")
              if (!error.isCorrect) {
                swal({title:"Sesión expirada", text:"Tu sesión a expirado, debes de volver a iniciar sesión", type:"info",allowOutsideClick: false})
                  .then(() => {
                    localStorage.clear();
                    this._router.navigate(['/login']);
                    window.location.reload();
                    this._userService.isLogged = false
                  });
  
  
              }
            }
          } catch (error) {
            swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error")
          }
         
        }
      );
    }
  }
  
  validateData():Number{
    if(this.user.confirmPassword !== this.user.password){
      swal({title:'Error',text:'Las contraseñas no coinciden',type:"error",timer: 1000,showCancelButton:false,showConfirmButton:false});
      return 0;
    }
    else
      return 1;

  }

}
