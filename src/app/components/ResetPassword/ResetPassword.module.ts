import { ResetPasswordRoutingModule } from './ResetPassword-routing.module';
import { ResetPasswordComponent } from './ResetPassword.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    ResetPasswordRoutingModule
  ],
  declarations: [
    ResetPasswordComponent
]
})
export class ResetPasswordModule { }