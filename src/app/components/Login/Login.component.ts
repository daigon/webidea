import { Route, Router } from '@angular/router';
import { User } from './../../models/user';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
@Component({
  selector: 'app-Login',
  templateUrl: './Login.component.html',
  styleUrls: ['./Login.component.scss'],
})
export class LoginComponent implements OnInit {
  public user: User;
  public forgotPassword: boolean = false;
  constructor(
    private _userService: UserService,
    private _router: Router
  ) {
    if (localStorage.getItem('isLoggedin') === 'true') {
      this._router.navigate(['/showIdeas']);
    }
    this.user = new User();
  }

  ngOnInit() {
  }

  public onSubmit() {


    this._userService.signin(this.user).subscribe(
      response => {

        //conseguir los datos
        const identity: User = response;


        if (!identity._id) {
          swal('El usuario no se identifico bien');
        } else {


          // crear localstorage
          localStorage.setItem('identity', JSON.stringify(identity));
          // conseguir el token
          this._userService.signin(this.user, true).subscribe(
            response => {

              const token = response.token;

              if (token <= 0) {
                swal("Error",'El token no se a generado correctamente','error');
              }
              else {
                if (identity.notifyAddIde) {
                  swal("Idea creada", "Se creo una idea el cual eres parte del equipo", "info");
                }
                if (identity.notifyUpdateIdea) {
                  swal("Actualización de la idea", "Se actualizo una idea de la que eres parte", "info");
                }

                if (identity.notifyUpdateIdea || identity.notifyAddIde) {
                  this.updateUser(identity, identity._id, token);
                }
                localStorage.setItem('token', token);
                localStorage.setItem('isLoggedin', 'true');
                this._userService.isLogged = true;

                
                this._router.navigate(['/showIdeas'])
                this.user = new User();
              }
            },
            error => {
              const errorMessage = <any>error;
              if (errorMessage != null) {
                try {
                  const body = JSON.parse(error._body);
                  swal("Error",body.message,'error');
                } catch (error) {
                  swal("Error",'Error en el servidor, intente mas tarde','error');
                }
                
              }

            }
          );
        }
      },
      error => {
        const errorMessage = <any>error;
        if (errorMessage != null) {
          try {
            const body = JSON.parse(error._body);
            swal("Error",body.message,'error');
          } catch (error) {
            swal("Error",'Error en el servidor, intente mas tarde','error');
          }
          
        }
      }
    );
  }

  private updateUser(user: User, id: string, token) {
    user.notifyAddIde = false;
    user.notifyUpdateIdea = false;
    this._userService.UpdateUser(token, user, id).subscribe(
      res => {
        console.log("Notificacion enviada");
      },
      err => {
        const errorMessage = <any>err;
        if (errorMessage != null) {
          try {
            const body = JSON.parse(err._body);
            swal("Error",body.message,'error');
          } catch (error) {
            swal("Error",'Error en el servidor, intente mas tarde','error');
          }
          
        }
      }
    );
  }

  toggleGetPassword() {
    if (!this.forgotPassword) {
      this.forgotPassword = true;
    }
    else {
      this.forgotPassword = false;
    }

  }

  forgetPassword() {

    var email = { email: this.user.email };
    this._userService.forgotPassword(null, email).subscribe(
      res => {
        swal("Email enviado", res.message, "success");
        this.forgotPassword = false;
      },
      err => {
        try {
          console.log(err);
          var error = JSON.parse(err._body);
          swal("Email no registrado", error.message, "error");
        } catch (error) {

          swal("Error", "Ocurrio un error en el servicio intente mas tarde.", "error");
        }




      }
    );

  }

}
