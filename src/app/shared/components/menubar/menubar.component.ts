import { User } from './../../../models/user';

import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { GLOBAL } from './../../../services/global';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss'],
  providers: []
})
export class MenubarComponent implements OnInit {
  public menuItems: any[];
  public brandMenu: any;
  isCollapsed = true;
  public urlImage = GLOBAL.urlGetImageUser;
  // localstorage
  public identity: User;
  public token;

  constructor(public _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router) {
    this.identity = this._userService.getIdentity();
    
    this.token = this._userService.getToken();

  }

  ngOnInit() {

  }

  public get menuIcon(): string {
    return this.isCollapsed ? '☰' : '✖';
  }

  public getMenuItemClasses(menuItem: any) {

  }

  public logout() {

    
    localStorage.clear();
    this.identity = null;
    this.token = null;
    
    this._router.navigate(['/']);
    this._userService.isLogged = false;

  }


}
